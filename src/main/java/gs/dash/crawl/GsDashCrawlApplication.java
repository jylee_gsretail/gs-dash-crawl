package gs.dash.crawl;

import gs.dash.crawl.password.SHA256;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import gs.dash.crawl.config.ShutdownHookConfig;


@SpringBootApplication
@Slf4j
public class GsDashCrawlApplication {

    public static void main(String[] args) {
        SpringApplication.run(GsDashCrawlApplication.class, args).close();
        log.info("=========== Server Start ===========");
    }

    @Bean(destroyMethod = "destroy")
    public ShutdownHookConfig shutdownHookConfig() {
        return new ShutdownHookConfig();
    }

    /*
    SHA256 sha256 = new SHA256();

    //비밀번호
    String password = "hi12345678";
    //SHA256으로 암호화된 비밀번호
    String cryptogram = sha256.encrypt(password);

    log.info(cryptogram);

    //비밀번호 일치 여부
    log.info(cryptogram.equals(sha256.encrypt(password)));
    */
}
