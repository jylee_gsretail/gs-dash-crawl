package gs.dash.crawl.job.bd;

import gs.dash.crawl.entity.bd.PrdBrandM;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bd.BdPrdBrandMRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BdPrdBrandMJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BdPrdBrandMRepository bdPrdBrandMRepository;

    @Value("#{system['aws.s3.public.url']}")
    private String S3_URL;

    private String AWS_S3_DIR ="D:/workspace/GIP/gip-bt/src/main/resources/s3";

    @Value("#{system['aws.s3.file.extension']}")
    private String SAVE_FILE_EXE;

    @Value("#{system['aws.s3.delimiter']}")
    private String DELIMITER;

    @Value("#{system['aws.s3.retry.cnt']}")
    private Integer RETRY_CNT;

    private final String SAVE_FILE_PATH = "s3/"+ LocalDate.now().getYear() + "/" + LocalDate.now().getMonthValue();

    @Bean(name = "bdPrdBrandMjob")
    public Job bdPrdBrandMjob(JobCompletionNotificationListener listener, Step bdPrdBrandMStep) {
        return jobBuilderFactory.get("bdPrdBrandMjob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bdPrdBrandMStep)
                .end()
                .build();
    }

    @Bean(name = "bdPrdBrandMStep")
    public Step bdPrdBrandMStep(ItemWriter<PrdBrandM> bdPrdBrandMWriter) throws Exception {
        return stepBuilderFactory.get("bdPrdBrandMStep")
                .<PrdBrandM, PrdBrandM> chunk(20)
                .reader(bdPrdBrandMReader())
                .writer(bdPrdBrandMWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<PrdBrandM> bdPrdBrandMReader() {
        String SAVE_FILE_NAME = "BD_PRD_BRAND_M.csv";
        File file = new File(AWS_S3_DIR +"/" + SAVE_FILE_NAME);

        try{
            log.info("[bdPrdBrandMReader] : Create Download File ...");
            if(!file.exists()){
                file.createNewFile();
            }
            log.info("[bdPrdBrandMReader] : Complete File !!! .. {}",file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error("[bdPrdBrandMReader] : Error!! - Permission Denied When creating Resource File in 'step -> reader', Plz Check mask ,,, {}",file.getAbsolutePath());
        }catch(IOException ioe){
            log.error("[bdPrdBrandMReader] : I/O Error When invoked ItemReader,{} , {}",ioe.getMessage(),file.getAbsolutePath());
        }catch(Exception e){
            log.error("[bdPrdBrandMReader] : UnExpected Error When invoked ItemReader,{}",e.getMessage());
        }
        finally {
            log.info("[bdPrdBrandMReader] : Start File Download from '{}' !",S3_URL);
            //FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, SAVE_FILE_NAME, SAVE_FILE_PATH, RETRY_CNT);
            log.info("[bdPrdBrandMReader] : Exit File Download..");
        }
        Long fs = file.length();
        /*
            - 파일이 정상적으로 생성 및 S3 url로 부터 정상적이게 데이터 파싱(크키가 0 초과)
            - 1일에 1번 초과 및 중복 요청 시 처리 추가해야하는지 ..?
         */
        log.info("[bdPrdBrandMReader] : Check File Size : {}",fs);

        if(Long.compare(fs,0L) > 0){
            try{
                return new FlatFileItemReaderBuilder<PrdBrandM>()
                        .linesToSkip(1)
                        .name("bdPrdBrandMReader")
                        .resource(new ClassPathResource("s3"+ File.separator + SAVE_FILE_NAME))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"brandCd", "brandNm", "etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<PrdBrandM>() {
                            @Override
                            public PrdBrandM mapFieldSet(FieldSet fieldSet) throws BindException {
                                String brandCd = fieldSet.readString(0);
                                String brandNm = fieldSet.readString(1);
                                String etlDtm = fieldSet.readString(2);
                                return new PrdBrandM(brandCd, brandNm, etlDtm);
                            }
                        })
                        .build();
            }catch(Exception e){
                log.error("[bdPrdBrandMReader] : Error When Parsing {}\n{}",file.getAbsolutePath(),e.getMessage());
                log.error("{}",e.getStackTrace());
                return new FlatFileItemReader<PrdBrandM>();
            }
        }else{
            log.info("[bdPrdBrandMReader] : File is Empty",fs);
            return null;
        }
    }

    @Bean
    public ItemWriter<PrdBrandM> bdPrdBrandMWriter() {
        log.info("[bdPrdBrandMWriter] Start Insert DB ... ");
        return ((List<? extends PrdBrandM> list) ->{
            bdPrdBrandMRepository.saveAll(list);
        }
        );

    }
}
