package gs.dash.crawl.job.bd;

import gs.dash.crawl.entity.bd.PrdPrdM;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bd.BdPrdPrdMRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BdPrdPrdMJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BdPrdPrdMRepository bdPrdPrdMRepository;

    @Value("#{system['aws.s3.public.url']}")
    private String S3_URL;

    private String AWS_S3_DIR ="D:/workspace/GIP/gip-bt/src/main/resources/s3";

    @Value("#{system['aws.s3.file.extension']}")
    private String SAVE_FILE_EXE;

    @Value("#{system['aws.s3.delimiter']}")
    private String DELIMITER;

    @Value("#{system['aws.s3.retry.cnt']}")
    private Integer RETRY_CNT;

    private final String SAVE_FILE_PATH = "s3/"+ LocalDate.now().getYear() + "/" + LocalDate.now().getMonthValue();

    @Bean(name = "bdPrdPrdMjob")
    public Job bdPrdPrdMjob(JobCompletionNotificationListener listener, Step bdPrdPrdMStep) {
        return jobBuilderFactory.get("bdPrdPrdMjob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bdPrdPrdMStep)
                .end()
                .build();
    }

    @Bean(name = "bdPrdPrdMStep")
    public Step bdPrdPrdMStep(ItemWriter<PrdPrdM> bdPrdPrdMWriter) throws Exception {
        return stepBuilderFactory.get("bdPrdPrdMStep")
                /*
                TO-DO :: 1D 배치지지만, 어느정도의 chunk 사이즈 측정이 필요하지 않나..? ( 샘플 데이터가 2년치라 현재로썬 상용에 맞는 chunk 사이즈 측정 힘듬 )
                 */
                .<PrdPrdM, PrdPrdM> chunk(20)
                .reader(bdPrdPrdMReader())
                .writer(bdPrdPrdMWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<PrdPrdM> bdPrdPrdMReader() {
        String SAVE_FILE_NAME = "BD_PRD_PRD_M.csv";
        File file = new File(AWS_S3_DIR +"/" + SAVE_FILE_NAME);

        try{
            log.info("[bdPrdPrdMReader] : Create Download File ...");
            if(!file.exists()){
                file.createNewFile();
            }
            log.info("[bdPrdPrdMReader] : Complete File !!! .. {}",file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error("[bdPrdPrdMReader] : Error!! - Permission Denied When creating Resource File in 'step -> reader', Plz Check mask ,,, {}",file.getAbsolutePath());
        }catch(IOException ioe){
            log.error("[bdPrdPrdMReader] : I/O Error When invoked ItemReader,{} , {}",ioe.getMessage(),file.getAbsolutePath());
        }catch(Exception e){
            log.error("[bdPrdPrdMReader] : UnExpected Error When invoked ItemReader,{}",e.getMessage());
        }
        finally {
            log.info("[bdPrdPrdMReader] : Start File Download from '{}' !",S3_URL);
            //FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, SAVE_FILE_NAME, SAVE_FILE_PATH, RETRY_CNT);
            log.info("[bdPrdPrdMReader] : Exit File Download..");
        }
        Long fs = file.length();
        /*
        TO-DO :: reader 에서 에러 ( ex : Excel Format 으로 인한 Parsing 에러  , 혹은 SqlException -> Duplicatie)
         */
        log.info("[bdPrdPrdMReader] : Check File Size : {}",fs);
        return Long.compare(fs,0L) > 0  ?
                new FlatFileItemReaderBuilder<PrdPrdM>()
                        .linesToSkip(1)
                        .name("bdPrdPrdMReader")
                        .resource(new ClassPathResource("s3"+ File.separator + SAVE_FILE_NAME))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"prdCd", "prdNm", "prdStsCd", "supCd",
                                             "brandCd", "sectCd", "etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<PrdPrdM>() {
                            @Override
                            public PrdPrdM mapFieldSet(FieldSet fieldSet) throws BindException {
                                String prdCd = fieldSet.readString(0);
                                String prdNm = fieldSet.readString(1);
                                String prdStsCd = fieldSet.readString(2);
                                String supCd = fieldSet.readString(3);
                                String brandCd = fieldSet.readString(4);
                                String sectCd = fieldSet.readString(5);
                                String etlDtm = fieldSet.readString(6);
                                return new PrdPrdM(prdCd, prdNm, prdStsCd, supCd,
                                                   brandCd, sectCd, etlDtm);
                            }
                        })
                        .build()
                : null ;

    }

    @Bean
    public ItemWriter<PrdPrdM> bdPrdPrdMWriter() {
        log.info("[bdPrdPrdMWriter] Start Insert DB ... ");
        return ((List<? extends PrdPrdM> list) ->{
            bdPrdPrdMRepository.saveAll(list);
        }
        );

    }
}
