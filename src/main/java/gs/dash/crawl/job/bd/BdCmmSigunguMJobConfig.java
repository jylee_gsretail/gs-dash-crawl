package gs.dash.crawl.job.bd;

import gs.dash.crawl.entity.bd.CmmSigunguM;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bd.BdCmmSigunguMRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BdCmmSigunguMJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BdCmmSigunguMRepository bdCmmSigunguMRepository;

    @Value("#{system['aws.s3.public.url']}")
    private String S3_URL;

    private String AWS_S3_DIR ="D:/workspace/GIP/gip-bt/src/main/resources/s3";

    @Value("#{system['aws.s3.file.extension']}")
    private String SAVE_FILE_EXE;

    @Value("#{system['aws.s3.delimiter']}")
    private String DELIMITER;

    @Value("#{system['aws.s3.retry.cnt']}")
    private Integer RETRY_CNT;

    private final String SAVE_FILE_PATH = "s3/"+ LocalDate.now().getYear() + "/" + LocalDate.now().getMonthValue();

    @Bean(name = "bdCmmSigugunMjob")
    public Job bdCmmSigugunMjob(JobCompletionNotificationListener listener, Step bdCmmSigugunMStep) {
        return jobBuilderFactory.get("bdCmmSigugunMjob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bdCmmSigugunMStep)
                .end()
                .build();
    }

    @Bean(name = "bdCmmSigugunMStep")
    public Step bdCmmSigugunMStep(ItemWriter<CmmSigunguM> bdCmmSigugunMWriter) throws Exception {
        return stepBuilderFactory.get("bdCmmSigugunMStep")
                .<CmmSigunguM, CmmSigunguM> chunk(20)
                .reader(bdCmmSigugunMReader())
                .writer(bdCmmSigugunMWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<CmmSigunguM> bdCmmSigugunMReader() {
        String SAVE_FILE_NAME = "BD_CMM_SIGUNGU_M.csv";
        File file = new File(AWS_S3_DIR +"/" + SAVE_FILE_NAME);

        try{
            log.info("[bdCmmSigugunMReader] : Create Download File ...");
            if(!file.exists()){
                file.createNewFile();
            }
            log.info("[bdCmmSigugunMReader] : Complete File !!! .. {}",file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error("[bdCmmSigugunMReader] : Error!! - Permission Denied When creating Resource File in 'step -> reader', Plz Check mask ,,, {}",file.getAbsolutePath());
        }catch(IOException ioe){
            log.error("[bdCmmSigugunMReader] : I/O Error When invoked ItemReader,{} , {}",ioe.getMessage(),file.getAbsolutePath());
        }catch(Exception e){
            log.error("[bdCmmSigugunMReader] : UnExpected Error When invoked ItemReader,{}",e.getMessage());
        }
        finally {
            log.info("[bdCmmSigugunMReader] : Start File Download from '{}' !",S3_URL);
            //FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, SAVE_FILE_NAME, SAVE_FILE_PATH, RETRY_CNT);
            log.info("[bdCmmSigugunMReader] : Exit File Download..");
        }
        Long fs = file.length();
        /*
            - 파일이 정상적으로 생성 및 S3 url로 부터 정상적이게 데이터 파싱(크키가 0 초과)
            - 1일에 1번 초과 및 중복 요청 시 처리 추가해야하는지 ..?
         */
        log.info("[bdCmmSigugunMReader] : Check File Size : {}",fs);
        return Long.compare(fs,0L) > 0  ?
                new FlatFileItemReaderBuilder<CmmSigunguM>()
                        .linesToSkip(1)
                        .name("bdCmmSigugunMReader")
                        .resource(new ClassPathResource("s3"+ File.separator + SAVE_FILE_NAME))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"sigunguCd", "sigunguNm", "sidoCd", "etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<CmmSigunguM>() {
                            @Override
                            public CmmSigunguM mapFieldSet(FieldSet fieldSet) throws BindException {
                                String sigunguCd = fieldSet.readString(0);
                                String sigunguNm = fieldSet.readString(1);
                                String sidoCd = fieldSet.readString(2);
                                String etlDtm = fieldSet.readString(3);
                                return new CmmSigunguM(sigunguCd, sigunguNm, sidoCd, etlDtm);
                            }
                        })
                        .build()
                : null ;

    }

    @Bean
    public ItemWriter<CmmSigunguM> bdCmmSigugunMWriter() {
        log.info("[bdCmmSigugunMWriter] Start Insert DB ... ");
        return ((List<? extends CmmSigunguM> list) ->{
                bdCmmSigunguMRepository.saveAll(list);
            }
        );

    }

}
