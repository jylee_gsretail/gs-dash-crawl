package gs.dash.crawl.job.bd;


import gs.dash.crawl.entity.bd.CmmSidoM;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bd.BdCmmSidoMRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BdCmmSidoMJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BdCmmSidoMRepository bdCmmSidoMRepository;

    @Value("#{system['aws.s3.public.url']}")
    private String S3_URL;

    private String AWS_S3_DIR ="D:/workspace/GIP/gip-bt/src/main/resources/s3";

    @Value("#{system['aws.s3.file.extension']}")
    private String SAVE_FILE_EXE;

    @Value("#{system['aws.s3.delimiter']}")
    private String DELIMITER;

    @Value("#{system['aws.s3.retry.cnt']}")
    private Integer RETRY_CNT;

    private final String SAVE_FILE_PATH = "s3/"+LocalDate.now().getYear() + "/" + LocalDate.now().getMonthValue();

    @Bean(name = "bdCmmSidoMjob")
    public Job bdCmmSidoMjob(JobCompletionNotificationListener listener, Step bdCmmSidoMStep) {
        return jobBuilderFactory.get("bdCmmSidoMjob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bdCmmSidoMStep)
                .end()
                .build();
    }

    @Bean(name = "bdCmmSidoMStep")
    public Step bdCmmSidoMStep(ItemWriter<CmmSidoM> bdCmmSidoMWriter) throws Exception {
        return stepBuilderFactory.get("bdCmmSidoMStep")
                .<CmmSidoM, CmmSidoM> chunk(20)
                .reader(bdCmmSidoMReader())
                .writer(bdCmmSidoMWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<CmmSidoM> bdCmmSidoMReader() {
        String SAVE_FILE_NAME = "BD_CMM_SIDO_M.csv";
        File file = new File(AWS_S3_DIR +"/" + SAVE_FILE_NAME);

        try{
            log.info("[bdCmmSidoMReader] : Create Download File ...");
            if(!file.exists()){
                file.createNewFile();
            }
            log.info("[bdCmmSidoMReader] : Complete File !!! .. {}",file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error("[bdCmmSidoMReader] : Error!! - Permission Denied When creating Resource File in 'step -> reader', Plz Check mask ,,, {}",file.getAbsolutePath());
        }catch(IOException ioe){
            log.error("[bdCmmSidoMReader] : I/O Error When invoked ItemReader,{} , {}",ioe.getMessage(),file.getAbsolutePath());
        }catch(Exception e){
            log.error("[bdCmmSidoMReader] : UnExpected Error When invoked ItemReader,{}",e.getMessage());
        }
        finally {
            log.info("[bdCmmSidoMReader] : Start File Download from '{}' !",S3_URL);
            //FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, SAVE_FILE_NAME, SAVE_FILE_PATH, RETRY_CNT);
            log.info("[bdCmmSidoMReader] : Exit File Download..");
        }
        Long fs = file.length();
        /*
            - 파일이 정상적으로 생성 및 S3 url로 부터 정상적이게 데이터 파싱(크키가 0 초과)
            - 1일에 1번 초과 및 중복 요청 시 처리 추가해야하는지 ..?
         */
        log.info("[bdCmmSidoMReader] : Check File Size : {}",fs);
        return Long.compare(fs,0L) > 0  ?
                new FlatFileItemReaderBuilder<CmmSidoM>()
                        .linesToSkip(1)
                        .name("bdCmmSidoMReader")
                        .resource(new ClassPathResource("s3"+ File.separator + SAVE_FILE_NAME))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"sidoCd", "sidoNm", "etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<CmmSidoM>() {
                            @Override
                            public CmmSidoM mapFieldSet(FieldSet fieldSet) throws BindException {
                                String sidoCd = fieldSet.readString(0);
                                String sidoNm = fieldSet.readString(1);
                                String etlDtm = fieldSet.readString(2);
                                return new CmmSidoM(sidoCd,sidoNm, etlDtm);
                            }
                        })
                        .build()
                : null ;

    }

    @Bean
    public ItemWriter<CmmSidoM> bdCmmSidoMWriter() {
        log.info("[bdCmmSidoMWriter] Start Insert DB ... ");
        return ((List<? extends CmmSidoM> list) ->{
                bdCmmSidoMRepository.saveAll(list);
            }
        );

    }

}