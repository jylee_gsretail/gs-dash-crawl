package gs.dash.crawl.job.bf;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BfCstOrdDJobConfig {
}


