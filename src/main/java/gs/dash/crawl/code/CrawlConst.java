package gs.dash.crawl.code;

public class CrawlConst {
    public final static String PRODUCT_TOTAL_JOB_NAME = "gsDashCrawlJob";
    public final static int DATA_STEP_CHUNK_SIZE = 20;
}
