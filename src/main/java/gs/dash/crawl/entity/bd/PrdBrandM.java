package gs.dash.crawl.entity.bd;

import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name="BD_PRD_BRD_M")
public class PrdBrandM {
    @Id
    @Column
    private String brandCd;

    @Column
    private String brandNm;

    @Column
    private String etlDtm;

    @Builder
    public PrdBrandM(String brandCd, String brandNm, String etlDtm){
        this.brandCd = brandCd;
        this.brandNm = brandNm;
        this.etlDtm = etlDtm;
    }
}
