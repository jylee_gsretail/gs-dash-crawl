package gs.dash.crawl.entity.bd;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Table(name = "BD_SECT_SECT_M")
public class SectSectM {
    @Id
    @Column
    private String sectCd;

    @Column
    private String sectNm;

    @Column
    private String sectLvlCd;

    @Column
    private String sectLCd;

    @Column
    private String sectLNm;

    @Column
    private String sectMCd;

    @Column
    private String sectMNm;

    @Column
    private String sectSCd;

    @Column
    private String sectSNm;

    @Column
    private String etlDtm;

    @Builder
    public SectSectM(String sectCd, String sectNm, String sectLvlCd, String sectLCd,
                     String sectLNm, String sectMCd, String sectMNm, String sectSCd,
                     String sectSNm, String etlDtm){
            this.sectCd = sectCd;
            this.sectNm = sectNm;
            this.sectLvlCd = sectLvlCd;
            this.sectLCd = sectLCd;
            this.sectLNm = sectLNm;
            this.sectMCd = sectMCd;
            this.sectMNm = sectMNm;
            this.sectSCd = sectSCd;
            this.sectSNm = sectSNm;
            this.etlDtm = etlDtm;
    }
}
