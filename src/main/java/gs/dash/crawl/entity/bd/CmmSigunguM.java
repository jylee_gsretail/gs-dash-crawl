package gs.dash.crawl.entity.bd;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "BD_CMM_SIGUNGU_M")
public class CmmSigunguM {

    @Id
    @Column
    private String sigunguCd;

    @Column
    private String sigunguNm;

    @Column
    private String sidoCd;

    @Column
    private String etlDtm;

    @Builder
    public CmmSigunguM(String sigunguCd, String sigunguNm, String sidoCd, String etlDtm){
        this.sigunguCd = sigunguCd;
        this.sigunguNm = sigunguNm;
        this.sidoCd = sidoCd;
        this.etlDtm = etlDtm;
    }
}
