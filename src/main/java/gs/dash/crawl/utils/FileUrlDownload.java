package gs.dash.crawl.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

@Slf4j
public class FileUrlDownload {
    /**
     * 버퍼 사이즈
     */

    final static int size = 1024;

    /**
     * @param fileAddress
     * @param downloadDir
     */


    /**
     * fileAddress에서 파일을 읽어, 다운로드 디렉토리에 다운로드
     *
     * @param fileAddress
     * @param localFileName
     * @param downloadDir
     */


    /**
     * fileAddress에서 파일을 읽어, 다운로드 디렉토리에 다운로드, 다운로드 실패 시 재시도 횟수만큼 재시도
     *
     * @param fileUrl
     * @param tryCnt
     */

    public static boolean fileUrlReadAndDownload(String className, String fileUrl, File file, int tryCnt) {
        final int BUFFER_SIZE = 8192;


        OutputStream outputStream;
        InputStream inputStream;

        try {
            URL url = new URL(fileUrl);
            outputStream = new BufferedOutputStream(new FileOutputStream(file));
            inputStream = url.openConnection().getInputStream();
        } catch (MalformedURLException e) {
            log.info("URL Error (FileDownLoad)");
            sendWebExMessage(className, -1);
            return false;
        } catch (FileNotFoundException e) {
            log.info("File Not Found (FileDownLoad)");
            sendWebExMessage(className, -1);
            return false;
        } catch (IOException e) {
            log.info("Connection Error (FileDownLoad)");
            sendWebExMessage(className, -1);
            return false;
        }

        for (int i = 0; i < tryCnt; i++) {
            try {
                log.info("------- Download Start (try : " + (i + 1) + ") ------");

                int byteRead;
                int byteWritten = 0;
                int bytesBuffered = 0;
                byte[] buf = new byte[BUFFER_SIZE];

                while ((byteRead = inputStream.read(buf)) > -1) {
                    outputStream.write(buf, 0, byteRead);
                    bytesBuffered += byteRead;
                    byteWritten = bytesBuffered;

                    if(bytesBuffered > 1024*1024){
                        bytesBuffered = 0;
                        outputStream.flush();
                    }
                }

                log.info("Download Successfully.");
                log.info("File name : " + file.getName());
                log.info("of bytes  : " + byteWritten);
                log.info("------- Download End (try : " + (i + 1) + ") --------");

                sendWebExMessage(className, byteWritten);

                return true;

            } catch (Exception e) {
                log.error("Download failed (try : " + (i + 1) + " ), wait for 3sec and retry download");
                log.error(e.getMessage(), e);
                try {
                    Thread.sleep(3 * 1000);
                } catch (InterruptedException ie) {
                    log.info("Thread Sleep Error (FileDownLoad)");
                }
            }
        }
        try{
            outputStream.close();
            if (inputStream != null) inputStream.close();
        } catch (IOException ignored){
            return false;
        }

        sendWebExMessage(className, -1);
        return false;
    }
    /**
     * Download S3 Data File using URL through NIO
     * @param filePath
     * @param url
     * @param retryCount
     * @return success
     */
    public static boolean downloadFileFromUrl( String filePath, String url, int retryCount) {
        URL urlObj;
        ReadableByteChannel readableByteChannel = null;
        FileOutputStream fileOutputStream  = null;
        File file = new File(filePath);
        log.info("###########downloadFileFromUrl filePaht:{}",filePath);

        if(file.exists()) {
            try {
                urlObj = new URL(url);
                readableByteChannel = Channels.newChannel(urlObj.openStream());
                fileOutputStream = new FileOutputStream(filePath);
                for (int i = 0; i < retryCount; i++) {
                    try {
                        showLog(0, "------- Download Start (try : " + (i + 1) + ") ------");

                        long writedFileSize = fileOutputStream.getChannel()
                                .transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                        showLog(0, "File name : " + filePath);
                        showLog(0, "of bytes  : " + writedFileSize);
                        showLog( 0, "------- Download End (try : " + (i + 1) + ") --------");

                        sendWebExMessage(filePath, writedFileSize);

                        return true;

                    } catch (IOException e){
                        showLog(1,
                                "Download failed (try : " + (i + 1) + " ), wait for 3sec and retry download");
                        showLog(1, e.getMessage());
                        try {
                            Thread.sleep(3 * 1000);
                        } catch (InterruptedException ie) {
                            log.warn("InterruptedException 발생:{}",ie.getMessage(),ie);
                            showLog(1, "Thread Sleep Error.");
                        }
                    }
                }
            } catch (IOException ioExObj) {
                sendWebExMessage(filePath,-1);
                showLog(1, "Failed openning Channel.");
            } finally {
                try {
                    if(fileOutputStream != null){ fileOutputStream.close(); }
                    if(readableByteChannel != null) { readableByteChannel.close(); }
                } catch (IOException ioExObj) {
                    showLog( 1,
                            "Problem Occured While Closing The Object= " + ioExObj.getMessage());
                    return false;
                }
            }
        } else {
            showLog(1, "File Not Present! Please Check!");
        }
        return false;
    }

    /**
     * Show status of file download
     *
     * @param className
     * @param logType
     * @param message
     */
    private static void showLog(String className, int logType, String message){
       switch (logType){
           case 0:
               log.info("[File Download - " + className + "] " + message);
               break;
           case 1:
               log.error("[File Download - " + className + "] " + message);
               break;
       }
    }
    /**
     * Show status of file download
     *
     * @param logType
     * @param message
     */
    private static void showLog(int logType, String message){
        switch (logType){
            case 0:
                log.info("[File Download] " + message);
                break;
            case 1:
                log.error("[File Download] " + message);
                break;
        }
    }

    /**
     * Send a message about status of file download to the WebEx
     * @param className or filePath
     * @param fileSize
     */
    private static void sendWebExMessage(String className, long fileSize){
        String xmlConfigPath = "classpath:appContext.xml";
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext(xmlConfigPath);
        /*
            File Download 단계 정보 담은 Bean
         */
        //BaseInformation baseInformation = ctx.getBean("baseInfo", BaseInformation.class);

        String success = "File Download Success! ";
        String fail = "File Download Failed! ";
        String size = " Size = " + fileSize + " ";
        String result;

        if(fileSize < 1){
            result = fail;
        } else {
            result = success + size;
        }

        String message = "" + className + " - " + result + "";

        /*
         download 처리 message send
         */
        //WebexMessageUtil.sendMessage(message, WebexMessageUtil.DPS, baseInformation.getPhase());
    }


    /**
     * fileAddress에서 파일을 읽어, 다운로드 디렉토리에 다운로드, 다운로드 실패 시 재시도 횟수만큼 재시도
     *
     * @param fileAddress
     * @param localFileName
     * @param downloadDir
     * @param tryCnt
     */
    public static void fileUrlReadAndDownloadS3(String fileAddress, String localFileName, String downloadDir, int tryCnt) {
        OutputStream outStream = null;
        URLConnection uCon = null;
        InputStream is = null;

        try {
            for(int i=0; i<tryCnt; i++){
                try {
                    log.info("------- Download Start (try : " + (i+1) + ") ------");

                    URL url;
                    byte[] buf;
                    int byteRead;
                    int byteWritten = 0;
                    url = new URL(fileAddress);
                    outStream = new BufferedOutputStream(new FileOutputStream(downloadDir + "/" + localFileName));

                    uCon = url.openConnection();
                    is = uCon.getInputStream();
                    buf = new byte[size];
                    while ((byteRead = is.read(buf)) != -1) {
                        outStream.write(buf, 0, byteRead);
                        byteWritten += byteRead;
                    }

                    log.info("Download Successfully.");
                    log.info("File name : " + localFileName);
                    log.info("of bytes  : " + byteWritten);
                    log.info("------- Download End (try : " + (i+1) + ") --------");

                    break;
                } catch (Exception e) {
                    log.error("Download failed (try : " + (i+1) + " ), wait for 3sec and retry download");
                    log.error(e.getMessage(), e);
                    try{
                        Thread.sleep(3*1000);
                    } catch (InterruptedException ie) {}
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (is != null) is.close();
                if (outStream != null) outStream.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

}

