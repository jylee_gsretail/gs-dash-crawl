package gs.dash.crawl.converter;

import gs.dash.crawl.code.CollectStateCode;
import javax.persistence.Converter;
import org.springframework.stereotype.Component;

@Component
@Converter(autoApply = true)
public class CollectStateCodeConverter extends AbstractBaseEnumConverter<CollectStateCode, String> {

    @Override
    protected CollectStateCode[] getDetailCodeList() {
        return CollectStateCode.values();
    }
}

