package gs.dash.crawl.repository.bd;

import gs.dash.crawl.entity.bd.CmmSigunguM;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdCmmSigunguMRepository extends JpaRepository<CmmSigunguM,Long> {
}
