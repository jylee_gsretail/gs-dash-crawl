package gs.dash.crawl.repository.bd;

import gs.dash.crawl.entity.bd.PrdBrandM;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdPrdBrandMRepository extends JpaRepository<PrdBrandM,Long> {
}
