package gs.dash.crawl.config;

import org.apache.logging.log4j.core.LoggerContext;
import org.slf4j.LoggerFactory;

public class ShutdownHookConfig {

    public void destroy() {

        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.stop();

    }
}
